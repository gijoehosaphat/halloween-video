import numpy as np
import cv2

# cap = cv2.VideoCapture('test.mov')
cap = cv2.VideoCapture(0)

# set Width
ret = cap.set(3, 596)
# set Height
ret = cap.set(4, 336)

# w = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
# h = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
# fps = 15
# fourcc = cv2.cv.CV_FOURCC('m', 'p', '4', 'v') # note the lower case
# out = cv2.VideoWriter("output/output.mov", fourcc, fps, (w, h), True)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
fgbg = cv2.createBackgroundSubtractorMOG2(history=500, varThreshold=16, detectShadows=True)

cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
cv2.namedWindow('mask', cv2.WINDOW_NORMAL)
cv2.namedWindow('res', cv2.WINDOW_NORMAL)

cv2.moveWindow('frame', 0, 0)
cv2.moveWindow('mask', 596, 0)
cv2.moveWindow('res', 0, 350)

#-----------------------------------------------------------------------------
#       Load and configure ghost
#-----------------------------------------------------------------------------
ghost = cv2.VideoCapture('ghost.mp4')
# set Width
ret = ghost.set(3, 596)
# set Height
ret = ghost.set(4, 336)
# img = cv2.imread('bg.png', -1)

while (1):
    ret, frame = cap.read()
    ret, ghost_frame = ghost.read()

    # height, width, depth = frame.shape
    # print height, width, depth
    #
    # height, width, depth = ghost_frame.shape
    # print height, width, depth

    # Frame
    cv2.imshow('frame', ghost_frame)

    # Mask
    mask = fgbg.apply(frame)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    cv2.imshow('mask', mask)

    # print ghost_frame[0][0]

    # Res
    move = cv2.bitwise_and(frame, frame, mask=mask)
    np.putmask(frame, ghost_frame>0, ghost_frame)
    np.putmask(frame, move>0, move)
    # move = cv2.bitwise_and(frame, frame, mask=mask)
    # background = np.concatenate(frame, ghost_frame)
    # background = cv2.bitwise_or(frame, ghost_frame)

    # background = cv2.addWeighted(frame, 1.0, ghost_frame, 1.0, 0.0)
    # res = cv2.addWeighted(background, 1.0, move, 1.0, 0.0)
    res = frame
    cv2.imshow('res', res)

    # break
    # out.write(frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# out.release()
cap.release()
cv2.destroyAllWindows()
